# Beer Stimulus

A simple [Stimulus](https://stimulusjs.org) demo app based on [stimulus-starter](https://github.com/stimulusjs/stimulus-starter). Data via [Punk API](https://punkapi.com).

---

You can [remix this project on Glitch](https://glitch.com/edit/#!/import/github/citizen428/beer-stimulus) if you want to play around with it without installing anything.

[![Remix on Glitch](https://cdn.glitch.com/2703baf2-b643-4da7-ab91-7ee2a2d00b5b%2Fremix-button.svg)](https://glitch.com/edit/#!/import/github/citizen428/beer-stimulus)
