import { Controller } from "stimulus"

export default class extends Controller {
  static targets = ["beer"]

  connect() {
    this.randomBeer(this.renderBeer);
  }

  updateBeer(e) {
    e.preventDefault();
    this.randomBeer(this.renderBeer);
  }

  randomBeer(callback) {
    fetch('https://api.punkapi.com/v2/beers/random')
    .then(response => response.text())
    .then(json => { callback.call(this, JSON.parse(json)[0]) })
  }

  renderBeer(beer) {
    const html = `
      <h2 class="title">${beer.name}</h2>
      <h3>${beer.tagline}</h3>
      <img src=${beer.image_url} height="200px" />
      <div>${beer.description} ABV: ${beer.abv}%</div>
    `;
    this.beerTarget.innerHTML = html;
  }

}
